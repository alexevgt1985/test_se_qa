import main
import pytest

# Test without params


class Test:

    """Тесты максимальных/минимальных значений"""
    def test_max(self):
        assert main.calc_bonus(100, 100, 100) == 0.30

    def test_min(self):
        assert main.calc_bonus(0, 0, 0) == 0.00

    '''Тесты пограничных значений без больничных'''
    def test_3_years_without_med(self):
        assert main.calc_bonus(3, 0, 0) == 0.30

    def test_1_5_years_without_med(self):
        assert main.calc_bonus(1.5, 0, 0) == 0.28

    def test_90_days_without_med(self):
        assert main.calc_bonus(0, 90, 0) == 0.15

    '''Тесты пограничных значений с больничными'''
    @pytest.mark.xfail
    def test_3_years_with_med(self):
        assert main.calc_bonus(3, 0, 1) == 0.30

    def test_1_5_years_with_med(self):
        assert main.calc_bonus(1.5, 0, 1) == 0.25

    def test_90_days_with_med(self):
        assert main.calc_bonus(0, 90, 1) == 0.18

    '''Тесты эквивалентных значений значений с больничными'''
    def test_high_3_years_without_med(self):
        assert main.calc_bonus(2, 900, 0) == 0.30

    def test_high_2_years_without_med(self):
        assert main.calc_bonus(1, 200, 0) == 0.25

    def test_near_1_5_years_without_med(self):
        assert main.calc_bonus(1.4, 100, 0) == 0.15

    '''Тесты эквивалентных значений значений с больничными'''
    def test_high_3_years_with_med(self):
        assert main.calc_bonus(2, 900, 1) == 0.30

    def test_high_2_years_with_med(self):
        assert main.calc_bonus(1, 200, 1) == 0.28

    def test_near_1_5_years_with_med(self):
        assert main.calc_bonus(1.4, 50, 1) == 0.18

    '''Тесты эквивалентных значений строковые параметры '''

    def test__a__bc(self):
        assert main.calc_bonus("a", 900, 0) is None

    def test_a__b__c(self):
        assert main.calc_bonus(1, "b", 0) is None

    def test_ab__c(self):
        assert main.calc_bonus(1, 90, "c") is None

    def test__a__bc(self):
        assert main.calc_bonus("3", 900, 0) is None
