import main
import pytest

# Test without params


@pytest.mark.parametrize("year_work, day_work, day_med, expected", [(100, 100, 100, 0.30),
                                                                    (0, 0, 0, 0.00),

                                                                    (3, 0, 0, 0.30),
                                                                    (1.5, 0, 0, 0.28),
                                                                    (0, 90, 0, 0.18),

                                                                    (3, 0, 1, 0.30),
                                                                    (1.5, 0, 1, 0.25),
                                                                    (0, 90, 1, 0.15),

                                                                    (2, 900, 0, 0.30),
                                                                    (2, 0, 1, 0.25),
                                                                    (1, 200, 0, 0.18),
                                                                    (1, 200, 1, 0.18),
                                                                    (1.5, 0, 0, 0.28),
                                                                    (1.5, 0, 1, 0.25),
                                                                    (1.4, 100, 1, 0.15)])
class Test:
    def test(self, year_work, day_work, day_med, expected):
        assert main.calc_bonus(year_work, day_work, day_med) == expected
