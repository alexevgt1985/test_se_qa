# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

"""
Напишите код-решение для задачи и покройте своё решение автотестами.

Задача: Напишите функцию расчета премии (премия процент суммы от заработанного за год):

если сотрудник работает более 3 лет она составляет 30% от годового дохода
если от 1,5 до 3 лет, то премия 25%
если от 90 дней до 1,5 лет, то 15%
если менее 90 дней, то премия не выплачивается.
если сотрудник не брал больничный за последний год, то дополнительно добавляется 3%
Например, Иван болел, но работает 10 лет - его премия 30% от годовой зарплаты
Никита не болел, работает 2 года - его премия 28% от годовой зарплаты

Ответ приложите в виде ссылки на репозиторий;

"""


def calc_bonus(year_work: float = 0, day_work: int = 0, day_med: int = 0) -> float:
    bonus_summary: float = 0.00
    # если сотрудник работает более 3 лет она составляет 30% от годового дохода
    try:
        if year_work >= 3:
            bonus_summary = 0.30
        # если от 1,5 до 3 лет, то премия 25%
        elif 1.5 <= year_work < 3:
            bonus_summary = 0.25
        # если от 90 дней до 1,5 лет, то 15%
        elif (1.5 < year_work) and (day_work >= 90):
            bonus_summary = 0.15
        # если сотрудник не брал больничный за последний год, то дополнительно добавляется 3%
        if (day_med == 0) and (1 < year_work < 3):
            bonus_summary += 0.03
        result = bonus_summary.__round__(2)
        print(result)
        return result
    except TypeError:
        print("Неправильны тип данных")

if __name__ == '__main__':
    calc_bonus(0, 100, 1)
